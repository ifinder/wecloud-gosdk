package heart

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

// 启动自动心跳
type StartAutoHeartBeatReq struct {
	Wcid string `json:"wcid"`
}


type StartAutoHeartBeatResp struct {
	base.WecloudResponse
	Data bool `json:"data"`
}

func (this *StartAutoHeartBeatResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
