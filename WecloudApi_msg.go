package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

// 个人同步微信消息
func (this *WecloudApi) SyncMessage(token string,req *SyncMessageReq) *SyncMessageResp {

	resp:=NewSyncMessageResp()

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/msg/sync_msg"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return resp
		}
	}

	return resp

}




// 发送文本消息
func (this *WecloudApi) SendTxtMessage(token string,req *SendTxtMessageReq) *SendTxtMessageResp {

	resp:=SendTxtMessageResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/msg/send_text_msg"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp

}


// 发送图片消息
func (this *WecloudApi) SendImageMessage(token string,req *SendImageMessageReq) *SendImageMessageResp {

	resp:=SendImageMessageResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/msg/send_image_msg"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}



// 发送图片消息
func (this *WecloudApi) SendVideoMessage(token string,req *SendVideoMessageReq) *SendVideoMessageResp {

	resp:=SendVideoMessageResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/msg/send_video_msg"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token,"Content-Type":"application/x-www-form-urlencoded"})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}



