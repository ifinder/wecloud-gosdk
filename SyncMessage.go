package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/msg"
	"strings"
)

type SyncMessageReq struct {
	Wcid string `json:"wcid"`
	Scene int `json:"scene"` // 同步消息 scene:1 synckey:1
	Synckey string `json:"synckey"`

}

type SyncMessageResp struct {
	base.WecloudResponse
	//Data []Message `json:"data"`
	Data SyncMessageData `json:"data"`
}

func NewSyncMessageResp()  *SyncMessageResp{
	item:=SyncMessageResp{

	}
	return &item
}

func (this *SyncMessageResp) FromJson(data []byte) error {

	return json.Unmarshal(data,this)
}


type SyncMessageData struct {
	ModUserInfos []interface{} `json:"ModUserInfos"`
	ModContacts []ModContacts `json:"ModContacts"`
	DelContacts []interface{} `json:"DelContacts"`
	UserInfoExts []interface{} `json:"UserInfoExts"`

	AddMsgs []Message `json:"AddMsgs"`

	Status int `json:"Status"`

}



const  (
	//
	MessageType_text=1 //1:文本消息
	MessageType_img=3 // 语音
	MessageType_voice=34 //视频
	MessageType_video=43
	MessageType_inventgroup=10002 // 10002: 群邀请
	MessageType_friendapply=37 // 好友申请

)

type Message struct {
	MsgId int64 `json:"MsgId"`
	FromUserName FromUserName `json:"FromUserName"`
	ToUserName ToUserName `json:"ToUserName"`
	MsgType int  `json:"MsgType"` // 37 好友验证请求
	Content Content `json:"Content"`
	Status int `json:"Status"`
	CreateTime int64 `json:"CreateTime"`
	ImgStatus int `json:"ImgStatus"`
	NewMsgId int64 `json:"NewMsgId"`
	MsgSource string `json:"MsgSource"`
	PushContent string  `json:"PushContent"` // 自己发消息 该字段是空

	Channel int `json:"Channel"` // -1 表示未知
	GroupId string `json:"GroupId"` //群消息 则： 群ID
	Self int `json:"Self"` //是否是自己发的消息
	//Wcid string `json:"wcid"` //session id
	ImgBuf ImgBuf `json:"ImgBuf"` //  发送图片数据 会有
}

type Content struct {
	String string `json:"string"`
}

type FromUserName struct {
	String string `json:"string"`
}

type ToUserName struct {
	String string `json:"string"`
}


type ImgBuf struct {
	ILen int `json:"iLen"`
	Buffer string `json:"Buffer"`
}



// 是否群里来的消息
func (this *Message) IsFromChatRoom() bool{
	if this.GroupId!=""{
		return true
	}
	is_chatromm:=strings.HasSuffix(this.FromUserName.String,"@chatroom")
	if !is_chatromm{
		is_chatromm=strings.HasSuffix(this.ToUserName.String,"@chatroom")
	}
	return is_chatromm
}

// 是否是群主发送的群消息
// 只需要判断 ToUserName 是群
func (this *Message) IsGroupOwnerSend() bool {
	is_chatromm:=strings.HasSuffix(this.ToUserName.String,"@chatroom")
	return is_chatromm
}

/*
群@： "String": "p125463:\n@finder-CTO-吴华龙 也"
普通： "String": "p125463:\nHdh"
 */
func (this *Message) IsAtMsg() bool{
	arr:=strings.Split(this.Content.String,":\n@")
	if len(arr)==2{
		return true
	}
	return false
}

// 来自谁发的消息
// who
func (this *Message) FromUserId() string{



	if this.IsAtMsg(){
		arr:=strings.Split(this.Content.String,":\n@")

		return arr[0]
	}else{
		arr:=strings.Split(this.Content.String,":\n")
		return arr[0]
	}
}

/*
// 不能使用
func (this *Message) FromNickname() string {
	if this.IsAtMsg(){
		arr:=strings.Split(this.Content.String,":\n@")
		return strings.Split(arr[1]," ")[0]
	}
	return ""
}*/

func (this *Message) IsAtMe() bool {
	idx:=strings.Index(this.PushContent,"@了你")
	if idx>=0{
		return true
	}
	return false
}


// 获取 真实消息内容
// 支持 群和私服
func (this *Message) GetWords() string{
	if this.IsAtMsg(){
		arr:=strings.Split(this.Content.String,":\n@")
		str:=arr[1]
		cs:=strings.Split(str," ") //这里不是空格 请注意， 特殊空格分隔

		content:=cs[1]

		return content
	}else{
		arr:=strings.Split(this.Content.String,":\n")
		if len(arr)==1{
			return arr[0]
		}else{
			return arr[1]
		}

	}
}


// 通用的消息解析
// 业务需要根据 msg_type 来转成 对应的业务对象
func (this *Message) Parse() (interface{},error){

	switch this.MsgType {
	case MessageType_text: // 文本消息
		return this.GetWords(),nil

	case MessageType_inventgroup: //群邀请消息

		var groupMsg GroupSysMsg

		group_id:=this.FromUserName.String // group_id
		content:=strings.Replace(this.Content.String,group_id+":","",1)
		err:=groupMsg.FromMsgContent(content)
		return groupMsg,err

	case MessageType_friendapply:

		var passVerify msg.MsgContentPassVerify
		content:=this.GetWords()
		err:=passVerify.Msg.FromXml([]byte(content))
		return passVerify,err

	}
	return nil,nil

}

// ========= ModContacts
// 联系人
type ModContacts struct {

	UserName UserName
	NickName NickName

	Sex int

}

type UserName struct {
	String string `json:"string"`
}

type NickName struct {
	String string `json:"string"`
}


