package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

type SendVideoMessageReq struct {
    ToWxIds string `json:"to_wx_ids"`

    Video_base64 string `json:"video_base64"`
	Cover_img_base64 string `json:"cover_img_base64"`
	Play_length int64 `json:"play_length"`

    Wcid string `json:"wcid"`
}


func (this *SendVideoMessageReq) SetToWxIds(to_wxids []string) {
	this.ToWxIds=strings.Join(to_wxids,",")
}




type SendVideoMessageResp struct {
	base.WecloudResponse
}

func (this *SendVideoMessageResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}





