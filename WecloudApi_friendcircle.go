package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/friendcircle"
)

// 朋友圈图片视频上传
func (this *WecloudApi) FriendCircleUpload(token string,req *friendcircle.FriendCircleUploadReq) *friendcircle.FriendCircleUploadResp {

	resp:=friendcircle.NewFriendCircleUploadResp()

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/friendcircle/upload"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token,"Content-Type":"application/x-www-form-urlencoded"})
	if err!=nil{
		resp.Code =base.Code_Err
		return resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return resp
		}
	}

	return resp

}


// 朋友圈发消息
func (this *WecloudApi) FriendCircleMessage(token string,req *friendcircle.FriendCircleMessageReq) *friendcircle.FriendCircleMessageResp {

	resp:=friendcircle.NewFriendCircleMessageResp()

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	req.SetContent() //把SnsMessage XML序列化 成xml 字符串

	uri:=this.serviceUrl+"/wc/friendcircle/message"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token,"Content-Type":"application/x-www-form-urlencoded"})
	if err!=nil{
		resp.Code =base.Code_Err
		return resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return resp
		}
	}

	return resp

}

