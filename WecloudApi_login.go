package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	wecloudgosdk "gitee.com/ifinder/wecloud-gosdk/login"
)

type WecloudApi struct {

	serviceUrl string
}

func NewWecloudApi(serviceUrl string)  *WecloudApi {

	api:=WecloudApi{serviceUrl:serviceUrl}
	return &api
}

// 应用的登陆 ； 可活动之后接口的TOKEN
func (this *WecloudApi) ApiLogin(req *ApiLoginReq) *ApiLoginResp {

	resp:=ApiLoginResp{}


	defer func() {
		if e:=recover();e!=nil{
			resp.Code = base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/login"

	ret,err:=gocomm.PostJSON(uri,req)
	if err!=nil{
		resp.Code=base.Code_Err
		resp.Msg = "请求失败"
		return &resp
	}

	err=resp.FromJson(ret)
	if err!=nil{
		resp.Code = base.Code_0
		resp.Msg = err.Error()
		return &resp
	}

	return &resp

}


//获取登陆二维码数据
func (this *WecloudApi) GetLoginQr(token string,req *GetLoginQrReq) *GetLoginQrResp {

	resp:= GetLoginQrResp{}


	defer func() {
		if e:=recover();e!=nil{
			resp.Code = base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/loginqr"

	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	//ret,_,err:=utils.PostJSONWithRespContentType(uri,string(req.ToJson()))
	//fmt.Printf(" 返回二维码数据 %s",string(ret))
	if err!=nil{
		resp.Code = base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code= base.Code_Err
			return &resp
		}
	}
	return &resp

}

// 检测是否登陆 并返回用户数据
func (this *WecloudApi) GetLoginInfo(token string,req *LoginInfoReq) *LoginInfoResp {


	resp:=LoginInfoResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/login_info"

	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})


	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp

}




/*
// Data62 静默登陆，当掉线后可以使用
func (this *WecloudApi) Data62Login(req Data62LoginReq) *Data62LoginResp {

	resp:=Data62LoginResp{Data:Data62LoginData{}}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =Code_1
		}
	}()

	uri:=this.serviceUrl+"/api/Login/Data62Login"

	res,err:=gocomm.PostJSON(uri,req)

	myJson, err := simplejson.NewJson(res)
	if err!=nil{
		panic(err)
	}

	//dataJson:=myJson.Get("data")   //获取不到 json 对象


	code,_:=myJson.Get("Code").String()
	msg,_:=myJson.Get("Message").String()

	resp.Code=code
	resp.Msg = msg

	datajson := myJson.Get("Data")
	ret,err:=datajson.Get("baseResponse").Get("ret").Int()
	if err!=nil{
		panic(err)
	}

	resp.Data.Ret=ret

	accountInfoJson := datajson.Get("accountInfo")

	wx_id,_:=accountInfoJson.Get("wxid").String()
	alias,_:=accountInfoJson.Get("Alias").String()

	resp.Data.Acc_id = wx_id
	resp.Data.Alias = alias

	return &resp

}
*/




// 二次登陆，当掉线后可以使用
func (this *WecloudApi) TwiceLogin(token string,req *TwiceLoginReq) *TwiceLoginResp {

	resp:=TwiceLoginResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/login_twice"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp

}



// 微信退出
func (this *WecloudApi) Logout(token string,req *LogoutReq) *LogoutResp {

	resp:=LogoutResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/logout"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}





// 二次登陆，当掉线后可以使用
func (this *WecloudApi) GetWxidByWcid(token string,req *wecloudgosdk.GetWxidByWcidReq) *wecloudgosdk.GetWxidByWcidResp {

	resp:=wecloudgosdk.GetWxidByWcidResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/login/get_wxid"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil {
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp

}

