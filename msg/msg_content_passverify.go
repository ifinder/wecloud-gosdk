package msg

import "encoding/xml"

// 解析微信同步消息为【验证好友申请】的XML数据
// MsgType: 37
type MsgContentPassVerify struct {
	Msg Msg `xml:"msg" json:"msg"`
}


func (this *Msg) FromXml(data []byte) error {
	err:= xml.Unmarshal(data,this)
	return err
}


type Msg struct {
	Fromusername string `xml:"fromusername,attr" json:"fromusername"`
	Encryptusername string `xml:"encryptusername,attr" json:"encryptusername"`
	Scene string `xml:"scene,attr" json:"scene"`
	City string `xml:"city,attr" json:"city"`
	Ticket string `xml:"ticket,attr" json:"ticket"`


	Opcode string `xml:"opcode,attr" json:"opcode"`

	Brandlist Brandlist `xml:"brandlist" json:"brandlist"`
}



type Brandlist struct {

	Count string `json:"count,attr" json:"count"`

}



