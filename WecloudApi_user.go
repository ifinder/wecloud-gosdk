package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/user"
)

// 获取个人微信二维码
func (this *WecloudApi) GetUserQr(token string,req *user.GetUserQrReq) *user.GetUserQrResp {

	resp:=user.GetUserQrResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/user/user_qr"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}


