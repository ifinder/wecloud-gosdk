package friendcircle

import (
	"encoding/xml"
	"fmt"
	"log"
	"testing"
)

func TestToXml(t *testing.T)  {

	//xml:=`<TimelineObject><id>13539658997753581712</id><username>dragon803040</username><createTime>1614053129</createTime><contentDesc>哈哈😄</contentDesc><contentDescShowType>0</contentDescShowType><contentDescScene>0</contentDescScene><private>0</private><sightFolded>0</sightFolded><showFlag>0</showFlag><appInfo><id></id><version></version><appName></appName><installUrl></installUrl><fromUrl></fromUrl><isForceUpdate>0</isForceUpdate></appInfo><sourceUserName></sourceUserName><sourceNickName></sourceNickName><statisticsData></statisticsData><statExtStr></statExtStr><ContentObject><contentStyle>15</contentStyle><title>微信小视频</title><description>Sight</description><mediaList><media><id>13539658998087422091</id><type>6</type><title></title><description>哈哈😄</description><private>0</private><userData></userData><subType>0</subType><videoSize width=\"\" height=\"\"></videoSize><url type=\"1\" md5=\"\" videomd5=\"\">http://shzjwxsns.video.qq.com/102/20202/snsvideodownload?filekey=30340201010420301e020166040253480410988c1f67c2ee6d3f2d4f5ad0db1b70180203201a4a040d00000004627466730000000131&amp;hy=SH&amp;storeid=32303231303232333132303532393030303361663666356461663038663432633664623430623030303030303636&amp;dotrans=2&amp;ef=15_0&amp;bizid=1023</url><thumb type=\"1\">http://vweixinthumb.tc.qq.com/150/20250/snsvideodownload?filekey=30340201010420301e0202009604025348041006064aeeb0e69eabc9b52181fdd53f5a02023926040d00000004627466730000000131&amp;hy=SH&amp;storeid=32303231303232333132303532393030303238373630356461663038663432633664623430623030303030303936&amp;bizid=1023</thumb><size width=\"\" height=\"\" totalSize=\"\"></size><videoDuration></videoDuration></media></mediaList><contentUrl>https://support.weixin.qq.com/cgi-bin/mmsupport-bin/readtemplate?t=page/common_page__upgrade&amp;v=1</contentUrl></ContentObject><actionInfo><appMsg><messageAction></messageAction></appMsg></actionInfo><location poiClassifyId=\"\" poiName=\"\" poiAddress=\"\" poiClassifyType=\"0\" city=\"\"></location><publicUserName></publicUserName><streamvideo><streamvideourl></streamvideourl><streamvideothumburl></streamvideothumburl><streamvideoweburl></streamvideoweburl></streamvideo></TimelineObject>`


	snsMessage:=SnsMessage{
		TimelineObject:TimelineObject{
			Id:                  "13539658997753581712",
			Username:            "dragon803040",
			CreateTime:          1614053129,
			ContentDesc:         "哈哈😄",
			ContentDescShowType: 0,
			ContentDescScene:    0,
			Private:             0,
			SightFolded:         0,
			ShowFlag:            0,
			AppInfo:             AppInfo{},
			ContentObject:       ContentObject{
				ContentStyle: 15,
				Title:        "微信小视频",
				Description:  "Sight",
				MediaList:    MediaList{[]Media{
					{
						Id:          "13539658998087422091",
						Type:        6,
						Description: "哈哈😄",
						Private:     0,
						UserData:    "",
						SubType:     0,
						VideoSize:   VideoSize{},
						Url:       Url{
							Type:     1,
							Videomd5: "",
							Url:      "http://shzjwxsns.video.qq.com/102/20202/snsvideodownload?filekey=30340201010420301e020166040253480410988c1f67c2ee6d3f2d4f5ad0db1b70180203201a4a040d00000004627466730000000131&amp;hy=SH&amp;storeid=32303231303232333132303532393030303361663666356461663038663432633664623430623030303030303636&amp;dotrans=2&amp;ef=15_0&amp;bizid=1023",
						},
						Thumb:      Thumb{
							Type:  1,
							Thumb: "http://vweixinthumb.tc.qq.com/150/20250/snsvideodownload?filekey=30340201010420301e0202009604025348041006064aeeb0e69eabc9b52181fdd53f5a02023926040d00000004627466730000000131&amp;hy=SH&amp;storeid=32303231303232333132303532393030303238373630356461663038663432633664623430623030303030303936&amp;bizid=1023",
						},
						Size:        Size{},
					},
				}},
			},
			Location:            Location{},
		},
	}

	d,err:=xml.Marshal(snsMessage.TimelineObject)
	if err!=nil{
		log.Fatal(err)
	}
	fmt.Printf("%s",d)


}
