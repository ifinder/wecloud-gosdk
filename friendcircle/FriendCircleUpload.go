package friendcircle

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

// 朋友圈上传图片
type FriendCircleUploadReq struct {
	Wcid string `json:"wcid"`

	Base64 string `json:"base64"`
}


type FriendCircleUploadResp struct {
	base.WecloudResponse
	Data FriendCircleUploadData `json:"data"`
}

func  NewFriendCircleUploadResp() *FriendCircleUploadResp {
	return &FriendCircleUploadResp{}
}

type FriendCircleUploadData struct {
	//BaseResponse base.BaseResponse `json:"BaseResponse"` //这个返回的补丁BaseResponse和 baseResponse
	StartPos int64
	TotalLen int64
	ClientId string

	BufferUrl ThumbUrls `json:"BufferUrl"`

	ThumbUrlCount int
	ThumbUrls []ThumbUrls

	Id int
	Type int
}

func (this *FriendCircleUploadResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
type ThumbUrls struct {
	Url string
}

