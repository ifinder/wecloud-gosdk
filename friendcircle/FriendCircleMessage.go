package friendcircle

import (
	"encoding/json"
	"encoding/xml"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

// 朋友圈上传图片
type FriendCircleMessageReq struct {
	Wcid string `json:"wcid"`

	Content string `json:"content"`

	Black_list string `json:"black_list"`

	With_user_list string `json:"with_user_list"`

	Is_video bool `json:"is_video"`


	SnsMessage SnsMessage // 不进行序列化 ，转成XML



}
func (this *FriendCircleMessageReq) SetContent() error {
	d,err:=xml.Marshal(this.SnsMessage.TimelineObject)
	if err!=nil{
		return err
	}
	this.Content  = string(d)

	this.Content=strings.ReplaceAll(this.Content,"\"","\\\"")

	return err
}




// ====== FriendCircleMessageResp
type FriendCircleMessageResp struct {
	base.WecloudResponse
	Data FriendCircleMessageData `json:"data"`
}

func  NewFriendCircleMessageResp() *FriendCircleMessageResp {
	return &FriendCircleMessageResp{}
}

func (this *FriendCircleMessageResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}



// ====== FriendCircleMessageData
type FriendCircleMessageData struct {

}


// ======== SnsMessage
type SnsMessage struct {
	// 属性得生成  必须使用 <a v=\"1\"> \" 才行 ，否则解析失败
	TimelineObject TimelineObject `xml:"TimelineObject"`
}




type TimelineObject struct {
	Id string `xml:"id"`
	Username string `xml:"username"`
	CreateTime int64 `xml:"createTime"`

	ContentDesc string `xml:"contentDesc"`

	ContentDescShowType int `xml:"contentDescShowType"`

	ContentDescScene int `xml:"contentDescScene"` // 图片 3 ； 视频:0

	Private int `xml:"private"`
	SightFolded int `xml:"sightFolded"`
	ShowFlag int `xml:"showFlag"`

	AppInfo AppInfo `xml:"appInfo"`

	ContentObject ContentObject `xml:"ContentObject"`
	Location Location `xml:"location"`
}

type AppInfo struct {

	Id string `xml:"id"`
	Version string `xml:"version"`
	AppName string `xml:"appName"`
	InstallUrl string `xml:"installUrl"`
	FromUrl string `xml:"fromUrl"`
	isForceUpdate int `xml:"isForceUpdate"`
}

type ContentObject struct {
	ContentStyle  int `xml:"contentStyle"` // 图片: 1 , 视频： 15
	Title string `xml:"title"`
	Description string `xml:"description"`

	MediaList MediaList `xml:"mediaList"`
}

type MediaList struct {
	Media []Media `xml:"media"`
}
type Media struct {
	Id string `xml:"id"`
	Type int `xml:"type"`   // 图片: 2 视频：6
	Title string `xml:"title"`
	Description string `xml:"description"`
	Private int `xml:"private"`
	UserData string `xml:"userData"`
	SubType int `xml:"subType"`
	VideoSize VideoSize `xml:"videoSize"`
	Url Url `xml:"url"`
	Thumb Thumb `xml:"thumb"`
	Size Size `xml:"size"`
}

type VideoSize struct {
	Width int `xml:"width,attr"`
	Height int `xml:"height,attr"`
}
type Url struct {
	Type int `xml:"type,attr"`
	Videomd5 string `xml:"videomd5,attr"`
	// content
	Url string `xml:",chardata"`
}
type Thumb struct {
	Type int `xml:"type,attr"`
    // content
	Thumb string `xml:",chardata"`
}
type Size struct {

}


type Location struct {

}
