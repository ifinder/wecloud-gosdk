package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type TwiceLoginReq struct {
	Wcid string `json:"wcid"`

}
type TwiceLoginResp struct {
	base.WecloudResponse
	Data TwiceLoginData `json:"data"`
}

type TwiceLoginData struct {

	Wcid string `json:"wcid"`
	Expire string `json:"expire"`

}


func (this *TwiceLoginResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
