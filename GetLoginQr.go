package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type GetLoginQrReq struct {

	base.WecloudRequest
	Wcid string `json:"wcid"`
}



func NewGetLoginQrReq() *GetLoginQrReq {
	req:=GetLoginQrReq{

	}
	return &req

}

func (this *GetLoginQrReq) ToJson() []byte {
	d,_:=json.Marshal(this)
	return d
}

const (

	Code_0 ="0"
	Code_1= "1"
)

type GetLoginQrResp struct {
	base.WecloudResponse
	Data QrData `json:"data"`
}

type QrData struct {
	Wcid string `json:"wcid"`
	Qr_data string `json:"qr_data"`
	Expire string `json:"expire"`
}

func (this *GetLoginQrResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
