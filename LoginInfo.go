package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type LoginInfoReq struct {
	Wcid string `json:"wcid"`

}
type LoginInfoResp struct {
	base.WecloudResponse
	Data LoginInfoData `json:"data"`
}

type LoginInfoData struct {
	Id            int64   `json:"id,string" db:"id" form:"id"`
	//Robot_code    string  `json:"robot_code" db:"robot_code" form:"robot_code"` // 机器人编号
	Robot_type    string  `json:"robot_type" db:"robot_type" form:"robot_type"`
	Create_time   int64   `json:"create_time" db:"create_time" form:"create_time"`
	Remark        string  `json:"remark" db:"remark" form:"remark"`
	Enabled       int     `json:"enabled" db:"enabled" form:"enabled"` // 0- 启用  1- 未启用
	Robot_name    string  `json:"robot_name" db:"robot_name" form:"robot_name"`
	Status        string  `json:"status" db:"status" form:"status"` // 0- 未登陆  1- 登陆  2- 掉线
	//Service_api   string  `json:"service_api" db:"service_api" form:"service_api"`
	S_accid       *string `json:"s_accid" db:"s_accid" form:"s_accid"`                   // 账号ID
	S_nickname    *string `json:"s_nickname" db:"s_nickname" form:"s_nickname"`          // 昵称
	S_state       *string `json:"s_state" db:"s_state" form:"s_state"`                   // 状态
	S_device      *string `json:"s_device" db:"s_device" form:"s_device"`                // 区分设备登陆
	S_headurl     *string `json:"s_headurl" db:"s_headurl" form:"s_headurl"`             // 头像
	S_mobile      *string `json:"s_mobile" db:"s_mobile" form:"s_mobile"`                // 手机号
	S_email       *string `json:"s_email" db:"s_email" form:"s_email"`
	S_alias       *string `json:"s_alias" db:"s_alias" form:"s_alias"`                       // 账号别名 ID
	S_data62      *string `json:"s_data62" db:"s_data62" form:"s_data62"`                    // 登陆的data62数据
	S_username    *string `json:"s_username" db:"s_username" form:"s_username"`              // 登陆账号
	S_password    *string `json:"s_password" db:"s_password" form:"s_password"`              // 登陆密码
	Src_type      int     `json:"src_type" db:"src_type" form:"src_type"`                    // 0-会员  1-内部用户
	Wcid  string   `json:"wcid" db:"wcid" form:"wcid"` // api节点
	App_id     string   `json:"app_id" db:"app_id" form:"app_id"`          // 关联用户
}

func (this *LoginInfoResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
