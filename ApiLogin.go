package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type ApiLoginReq struct {

	App_key string `json:"app_key"`
	App_secret string `json:"app_secret"`
}

type ApiLoginResp struct {
	base.WecloudResponse
	Data string `json:"data"`

}

func (this *ApiLoginResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
