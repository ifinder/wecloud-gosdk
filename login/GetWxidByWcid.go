package login


import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type GetWxidByWcidReq struct {
	Wcid string `json:"wcid"`

}
type GetWxidByWcidResp struct {
	base.WecloudResponse
	Data string `json:"data"`
}



func (this *GetWxidByWcidResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
