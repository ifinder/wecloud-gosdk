package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

type SendImageMessageReq struct {
    ToWxIds string `json:"to_wx_ids"`

    Base64 string `json:"content"`

    Wcid string `json:"wcid"`
}


func (this *SendImageMessageReq) SetToWxIds(to_wxids []string) {
	this.ToWxIds=strings.Join(to_wxids,",")
}




type SendImageMessageResp struct {
	base.WecloudResponse
}

func (this *SendImageMessageResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}





