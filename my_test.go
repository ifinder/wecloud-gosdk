package wecloudgosdk

import (
	"fmt"
	"strings"
	"testing"
)

/*


 {
        "MsgId": 1097420035,
        "FromUserName": {
          "String": "20039185157@chatroom"
        },
        "ToUserName": {
          "String": "dragon803040"
        },
        "MsgType": 1,
        "Content": {
          "String": "p125463:\n@finder-CTO-吴华龙 也"
        },
        "Status": 3,
        "ImgStatus": 1,
        "ImgBuf": {
          "iLen": 0,
          "Buffer": null
        },
        "CreateTime": 1596887389,
        "MsgSource": "<msgsource>\n\t<atuserlist><![CDATA[dragon803040]]></atuserlist>\n\t<silence>0</silence>\n\t<membercount>5</membercount>\n</msgsource>\n",
        "PushContent": "整 蔣海成在群聊中@了你",
        "NewMsgId": 5925864839599613000
      },

 */
func TestChatroom(t *testing.T)  {

	// 群消息
	s:="p125463:\n@finder-CTO-吴华龙 也 jias asdfji lo"

	arr:=strings.Split(s,":\n@")

	fmt.Println(arr)
	str:=arr[1]
	fmt.Println(len(str))
	idx:=strings.Split(str," ") //这里不是空格 请注意， 特殊空格分隔

	fmt.Println(idx)
}

func TestXmlFun(t *testing.T)   {

	x1:=`<sysmsg type="sysmsgtemplate">
	<sysmsgtemplate>
		<content_template type="tmpl_type_profile">
			<plain><![CDATA[]]></plain>
			<template><![CDATA["$username$"邀请"$names$"加入了群聊]]></template>
			<link_list>
				<link name="username" type="link_profile">
					<memberlist>
						<member>
							<username><![CDATA[youyouyiyeqiu]]></username>
							<nickname><![CDATA[郑^ ^ 前端]]></nickname>
						</member>
					</memberlist>
				</link>
				<link name="names" type="link_profile">
					<memberlist>
						<member>
							<username><![CDATA[wxid_n4vlza0is3rf22]]></username>
							<nickname><![CDATA[杨甜]]></nickname>
						</member>
					</memberlist>
					<separator><![CDATA[、]]></separator>
				</link>
			</link_list>
		</content_template>
	</sysmsgtemplate>
</sysmsg>
`
	x2:=`
<sysmsg type="sysmsgtemplate">
	<sysmsgtemplate>
		<content_template type="tmpl_type_profilewithrevoke">
			<plain><![CDATA[]]></plain>
			<template><![CDATA[你邀请"$names$"加入了群聊  $revoke$]]></template>
			<link_list>
				<link name="names" type="link_profile">
					<memberlist>
						<member>
							<username><![CDATA[wxid_0008590085622]]></username>
							<nickname><![CDATA[Rose]]></nickname>
						</member>
					</memberlist>
					<separator><![CDATA[、]]></separator>
				</link>
				<link name="revoke" type="link_revoke" hidden="1">
					<title><![CDATA[撤销]]></title>
					<usernamelist>
						<username><![CDATA[wxid_0008590085622]]></username>
					</usernamelist>
				</link>
			</link_list>
		</content_template>
	</sysmsgtemplate>
</sysmsg>
`

	fmt.Println(x1,"\n",x2)
	var sysMsg GroupSysMsg
	err := sysMsg.FromMsgContent(x1)
	fmt.Println(err)

}
