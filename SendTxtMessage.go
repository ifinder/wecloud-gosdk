package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

type SendTxtMessageReq struct {
    ToWxIds string `json:"to_wx_ids"`

    Content string `json:"content"`

	Wcid string `json:"wcid"`
}

type SendTxtMessageResp struct {
	base.WecloudResponse

}

func (this *SendTxtMessageResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}

func (this *SendTxtMessageReq) SetToWxIds(to_wxids []string) {
	this.ToWxIds=strings.Join(to_wxids,",")
}
