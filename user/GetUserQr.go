package user

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type GetUserQrReq struct {
	Wcid string `json:"wcid"`
}

type GetUserQrResp struct {

	base.WecloudResponse
	Data string `json:"data"`
}

func (this *GetUserQrResp)  FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
