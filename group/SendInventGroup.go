package group

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

// 发送群邀请 （ 适合40人以内）;
type SendInventGroupReq struct {
	ToWxIds string `json:"to_wx_ids"` // 被邀请的人 逗号隔开

	Group_id string `json:"group_id"` //群ID

	Wcid string `json:"wcid"`
}

func (this *SendInventGroupReq) SetToWxIds(to_wx_ids []string)  {
	this.ToWxIds=strings.Join(to_wx_ids,",")
}
//  双方必须是好友， 如果不是好友，那么状态返回0， 但是ErrMsg 是有问题的。 作为机器人应该打开用户加好友验证,自动通过，否则不能被要邀请进群
type SendInventGroupResp struct {
	base.WecloudResponse
	Data bool `json:"data"`
}


func (this *SendInventGroupResp)  FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
