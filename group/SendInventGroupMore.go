package group

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"strings"
)

// 发送群邀请 （ 适合40人以上）
type SendInventGroupMoreReq struct {
	ToWxIds string `json:"to_wx_ids"` // 被邀请的人

	Group_id string `json:"group_id"` //群ID

	Wcid string `json:"wcid"`
}


func (this *SendInventGroupMoreReq) SetToWxIds(to_wx_ids []string)  {
	this.ToWxIds=strings.Join(to_wx_ids,",")
}

type SendInventGroupMoreResp struct {
	base.WecloudResponse
	Data bool `json:"data"`
}

func (this *SendInventGroupMoreResp)  FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
