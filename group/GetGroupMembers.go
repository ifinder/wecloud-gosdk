package group

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

// 获取群成员资料
type GetGroupMembersReq struct {

	Group_id string `json:"group_id"`

	Wcid string `json:"wcid"`
}

type GetGroupMemberResp struct {
	base.WecloudResponse
	Data []ChatRoomMember `json:"data"`	//返回群ID
	Total_record int `json:"total_record"`
}


type ChatRoomMember struct {

	UserName string  `json:"UserName"` // wxid
	NickName string `json:"NickName"` // 微信昵称

	// DisplayName 暂时不支持
	// DisplayName *string `json:"displayName"` //群里面别人 如果不修改 则 null
	BigHeadImgUrl string `json:"BigHeadImgUrl"`
	SmallHeadImgUrl string `json:"SmallHeadImgUrl"`
	ChatroomMemberFlag int `json:"ChatroomMemberFlag"`
}

func (this *GetGroupMemberResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}



