package group

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

// 创建群
type CreateGroupReq struct {
	ToWxIds string `json:"to_wx_ids" ` // 指定人，至少1个; 并且不能与wxId 同名

	Group_name string `json:"group_name"` //该字段暂时不支持

	Wcid string `json:"wcid"`
}

type CreateGroupResp struct {

	base.WecloudResponse
	Data string `json:"data"`	//返回群ID
}


func (this *CreateGroupResp)  FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}


