package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/heart"
)

// 启动自动心跳
func (this *WecloudApi) StartAutoHeart(token string,req *heart.StartAutoHeartBeatReq) *heart.StartAutoHeartBeatResp {

	resp:=heart.StartAutoHeartBeatResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/heart/start_auto"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}


