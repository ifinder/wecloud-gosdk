package friend

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type ApiFriendPassVerifyReq struct {

	Scene int `json:"scene" form:"scene"` // 验证好友 来源

	V1  string `json:"v1" form:"v1"`  // 来源消息 encrytusername
	V2 string `json:"v2" form:"v2"`  // 来源消息 ticket
	Wcid string `json:"wcid" form:"wcid"`

}


type ApiFriendPassVerifyResp struct {
	base.WecloudResponse
	Data string `json:"data"` // 返回 username
}

func (this *ApiFriendPassVerifyResp) FromJson(data []byte) error {
	return json.Unmarshal(data,this)
}
