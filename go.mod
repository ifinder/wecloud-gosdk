module gitee.com/ifinder/wecloud-gosdk

go 1.14

require (
	gitee.com/ifinder/gocomm v0.0.0-20210122023711-a976842e33ea
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
