package wecloudgosdk

import (
	"encoding/json"
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type LogoutReq struct {
	Wcid string `json:"wcid"`

}
type LogoutResp struct {
	base.WecloudResponse
}

func (this *LogoutResp) FromJson(data []byte) error{
	return json.Unmarshal(data,this)
}
