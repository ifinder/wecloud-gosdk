package wecloudgosdk

import (
	"encoding/xml"
	"strings"
)

//解析微信同步消息为【邀请入群】的XML数据
type GroupSysMsg struct {
	SysMsgTemplate RspSysMsgTemplate `xml:"sysmsgtemplate" json:"sysmsgtemplate"`

}

type RspSysMsgTemplate struct {
	ContentTemplate RspContentTemplate `xml:"content_template" json:"content_template"`
}

type RspContentTemplate struct {
	Template string `xml:"template" json:"template"`
	LinkList RspLinkList `xml:"link_list" json:"link_list"`
}

type RspLinkList struct {
	Link []RspLink `xml:"link" json:"link"`



}


/*
name="names"
InventedLink RspLink `xml:"link" json:"link"`
// name="username"
InventorLink RspLink `xml:"link" json:"link"`
*/
type RspLink struct {
	MemberList RspMemberList `xml:"memberlist" json:"memberlist"`
	Name string `xml:"name,attr"`
}

type RspMemberList struct {
	Member []RqMember `xml:"member" json:"member"`
}

type RqMember struct {
	Nickname  string `xml:"nickname" json:"nickname"`
	Username  string `xml:"username" json:"username"`
}

func (this *GroupSysMsg) FromMsgContent(content string) error{
	err := xml.Unmarshal([]byte(content), this)
	return err
}

// 你邀请"$names$"加入了群聊  $revoke$
func (this *GroupSysMsg) IsInventUser() bool {
	bol := strings.Contains(this.SysMsgTemplate.ContentTemplate.Template, "邀请")
	return bol
}

// "$adder$"通过扫描你分享的二维码加入群聊  $revoke$
// adder
func (this *GroupSysMsg) IsGroupQrInventUser() bool {
	bol := strings.Contains(this.SysMsgTemplate.ContentTemplate.Template, "二维码加入")
	return bol
}



const LINKTYPE_INVENTOR = "username"
const LINKTYPE_INVENTEDS ="names"

const LINKTYPE_QR_INVENTOR="from"
const LINKTYPE_QR_INVENTEDS ="adder" // 二维码邀请进来的

// 获取被邀请的成员
func (this *GroupSysMsg) GetInventedMembers(link_type string) []RqMember {
	//

	links:=this.SysMsgTemplate.ContentTemplate.LinkList.Link
	if links!=nil && len(links)>0{

		for _,link:=range links{

			if link.Name == link_type{
				return link.MemberList.Member
			}
		}
	}
	return nil
}


