package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/group"
)

// 发送群邀请 40人已内
func (this *WecloudApi) SendInventGroup(token string,req *group.SendInventGroupReq) *group.SendInventGroupResp {

	resp:=group.SendInventGroupResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/group/send_invent_group"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}




// 发送群邀请 40人已上
func (this *WecloudApi) SendInventMoreGroup(token string,req *group.SendInventGroupMoreReq) *group.SendInventGroupMoreResp {

	resp:=group.SendInventGroupMoreResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/group/send_invent_group_more"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}




// 创建群
func (this *WecloudApi) CreateGroup(token string,req *group.CreateGroupReq) *group.CreateGroupResp {

	resp:=group.CreateGroupResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/group/create_group"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}




// 获取群成员资料
func (this *WecloudApi) GetGroupMembers(token string,req *group.GetGroupMembersReq) *group.GetGroupMemberResp {

	resp:=group.GetGroupMemberResp{}

	defer func() {
		if e:=recover();e!=nil{
			resp.Code =base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/group/group_members"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code =base.Code_Err
		return &resp
	}else{
		err=resp.FromJson(ret)
		if err!=nil{
			resp.Code=base.Code_Err
			return &resp
		}
	}
	return &resp
}
