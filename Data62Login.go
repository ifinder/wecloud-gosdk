package wecloudgosdk

import (
	"gitee.com/ifinder/wecloud-gosdk/base"
)

type Data62LoginReq struct {
	UserName string `json:"userName"`
	Password string `json:"password"`

	Data62 string `json:"data62"`
	ProxyIp string `json:"proxyIp"`
	ProxyUserName string `json:"proxyUserName"`

	ProxyPassword string `json:"proxyPassword"`
}



type Data62LoginResp struct {
	base.WecloudResponse
	Data Data62LoginData `json:"data"`
}

type Data62LoginData struct {
	Ret int `json:"ret"`
	Acc_id string `json:"acc_id"`
	Alias string `json:"alias"`
}



