package wecloudgosdk

import (
	"gitee.com/ifinder/gocomm"
	"gitee.com/ifinder/wecloud-gosdk/base"
	"gitee.com/ifinder/wecloud-gosdk/friend"
)



// 应用的登陆 ； 可活动之后接口的TOKEN
func (this *WecloudApi) PassVerify(token string,req *friend.ApiFriendPassVerifyReq) *friend.ApiFriendPassVerifyResp {

	resp:=friend.ApiFriendPassVerifyResp{}


	defer func() {
		if e:=recover();e!=nil{
			resp.Code = base.Code_Err
		}
	}()

	uri:=this.serviceUrl+"/wc/friend/pass_verify"
	ret,err:=gocomm.PostJSONForHeader(uri,req,map[string]string{"Access-Token":token})
	if err!=nil{
		resp.Code=base.Code_Err
		resp.Msg = "请求失败"
		return &resp
	}

	err=resp.FromJson(ret)
	if err!=nil{
		resp.Code = base.Code_0
		resp.Msg = err.Error()
		return &resp
	}

	return &resp

}


